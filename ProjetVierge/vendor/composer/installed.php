<?php return array(
    'root' => array(
        'name' => 'symfony/website-skeleton',
        'pretty_version' => 'v5.4.99',
        'version' => '5.4.99.0',
        'reference' => NULL,
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'symfony/flex' => array(
            'pretty_version' => 'v1.19.5',
            'version' => '1.19.5.0',
            'reference' => '51077ed0f6dc2c94cd0b670167eee3747c31b2c1',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../symfony/flex',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-iconv' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-php72' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/website-skeleton' => array(
            'pretty_version' => 'v5.4.99',
            'version' => '5.4.99.0',
            'reference' => NULL,
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
