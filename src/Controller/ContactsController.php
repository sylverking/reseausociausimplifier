<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ContactType;
use Symfony\Component\Routing\Annotation\Route;

class ContactsController extends AbstractController
{
    /**
     * @Route("/contacts", name="app_contacts")
     */
    public function index(Request $request, \Swift_Mailer $mailer): Response
    {

        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $contact = $form->getData();
            
            $message = (new \Swift_Message('Nouveau contact'))
                ->setFrom('sylverionomenjanahary@gmail.com')
                ->setTo($contact['Email'])
                ->setBody(
                    $this->renderView(
                        'Emails/contacts.html.twig', compact('contact')
                    ),
                    'text'
                );
            $mailer->send($message);

            $this->addFlash('Messsage', 'Le message a biens ete envoyer');
            // return $this->redirectToRoute('app_contacts');
        }

        return $this->render('contacts/index.html.twig', [
            'contactForm' => $form->createView()
        ]);
    }
}
