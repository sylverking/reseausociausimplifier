<?php

namespace App\Controller\Admin;

use App\Controller\HommeController;
use App\Entity\Articles;
use App\Entity\Informations;
use App\Entity\Comments;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(ArticlesCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('WorldTchat');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-blog');
        yield MenuItem::subMenu('Publication', 'fa fa-newspaper')->setSubItems([
            MenuItem::linkToCrud('Articles', 'fas fa-newspaper', Articles::class),
            MenuItem::linkToCrud('Ajouter articles', 'fas fa-plus', Articles::class)->setAction(Crud::PAGE_NEW)
        ]);
        yield MenuItem::linkToCrud('Commentaire', 'fas fa-comment', Comments::class);
        yield MenuItem::linkToCrud('profile', 'fas fa-user', User::class);
        yield MenuItem::linkToCrud('Informations', 'fas fa-palette', Informations::class);
        yield MenuItem::linkToRoute('Home', 'fas fa-sign-out-alt', 'app_home');
    }
}
