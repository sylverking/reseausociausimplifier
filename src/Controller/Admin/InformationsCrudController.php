<?php

namespace App\Controller\Admin;

use App\Entity\Informations;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController; 
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;

class InformationsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Informations::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            NumberField::new('age'),
            
            ImageField::new('image')->setUploadDir("public/asset/blog/images")
                                    ->setBasePath("/asset/blog/images")
                                    ->setRequired(false),
            AssociationField::new('author'),
        ];
    }
    
}
