<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Entity\Comments;
use App\Entity\Informations;
use Symfony\Component\Validator\Constraints\DateTime;
use App\Entity\User;
use App\Form\CommentsType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class HommeController extends AbstractController
{
    private $manager;

    public function __construct(EntityManagerInterface $em){
        $this->manager = $em;
    }
    /**
         * @Route("/home", name="app_home")
     */
    public function index(Request $request): Response
    {
        $repo = $this->getDoctrine()->getRepository(Articles::class);      
        $articles = $repo->findAll();

        $annonce = new Articles();
        
        $comments = new Comments();
        $commentsForm = $this->createForm(CommentsType::class, $comments);
        $commentsForm->handleRequest($request);

        if($commentsForm->isSubmitted() && $commentsForm->isValid()){
            
            return $this->redirectToRoute('app_home');
        }
        
        return $this->render('homePage/home.html.twig', [
            'articles' => $articles,
           
            'commentsForm' => $commentsForm->createView(),
        ]);
    }
}
?>